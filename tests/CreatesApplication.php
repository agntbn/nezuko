<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: CreatesApplication.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}
