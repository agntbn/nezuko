<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: TestCase.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
