<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: ExampleTest.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
