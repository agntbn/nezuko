<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: web.php
 * Last modified: 1/26/20, 10:50 AM
 */

/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: web.php
 * Last modified: 1/26/20, 6:56 AM
 */

/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: web.php
 * Last modified: 12/28/19, 6:11 AM
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/add', 'HomeController@add')->name('add');
Route::post('/add', 'HomeController@store')->name('store');

