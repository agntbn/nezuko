<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: EncryptCookies.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}
