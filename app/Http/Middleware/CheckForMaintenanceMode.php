<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: CheckForMaintenanceMode.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [
        //
    ];
}
