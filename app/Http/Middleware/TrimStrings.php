<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: TrimStrings.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;

class TrimStrings extends Middleware
{
    /**
     * The names of the attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        'password',
        'password_confirmation',
    ];
}
