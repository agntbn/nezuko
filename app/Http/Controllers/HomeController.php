<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: HomeController.php
 * Last modified: 1/26/20, 10:57 AM
 */

/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: HomeController.php
 * Last modified: 1/26/20, 6:56 AM
 */

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function add() {
        return view('add');
    }

    public function store(Request $request) {
        $member = new Member();

        $member->id         = $request->id;
        $member->nik        = $request->nik;
        $member->name       = $request->name;
        $member->job        = $request->job;
        $member->address    = $request->address;
        $member->email      = $request->email;
        $member->phone      = $request->phone;

        $member->save();

        return redirect()->route('home')->with('status', 'Berhasil menambah pengguna');
    }
}
