<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: AppServiceProvider.php
 * Last modified: 1/26/20, 9:41 AM
 */

/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: AppServiceProvider.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
       Schema::defaultStringLength(191);
    }
}
