<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: BroadcastServiceProvider.php
 * Last modified: 12/28/19, 6:11 AM
 */

namespace App\Providers;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes();

        require base_path('routes/channels.php');
    }
}
