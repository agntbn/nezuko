<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: Member.php
 * Last modified: 1/26/20, 10:33 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nik', 'name', 'job', 'address', 'email', 'phone',
    ];
}
