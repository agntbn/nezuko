<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: DatabaseSeeder.php
 * Last modified: 12/28/19, 6:11 AM
 */

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    }
}
