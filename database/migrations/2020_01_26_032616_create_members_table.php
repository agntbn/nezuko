<?php
/**
 * Author: Panji Setya Nur Prawira
 *
 * Filename: 2020_01_26_032616_create_members_table.php
 * Last modified: 1/26/20, 10:51 AM
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->bigInteger('nik');
            $table->string('name');
            $table->string('job');
            $table->string('address');
            $table->string('email')->unique();
            $table->string('phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
