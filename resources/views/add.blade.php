@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row justify-content-center">
  <div class="col-md-8">
   <div class="card">
    <div class="card-header">Add</div>

    <div class="card-body">
     <form method="POST" action="{{ route('store') }}">
      @csrf

      <div class="form-group row">
       <label for="id" class="col-md-4 col-form-label text-md-right">ID</label>

       <div class="col-md-6">
        <input type="text" class="form-control" name="id" value="{{ uniqid() }}" required autofocus readonly>
       </div>
      </div>

      <div class="form-group row">
       <label for="nik" class="col-md-4 col-form-label text-md-right">NIK</label>

       <div class="col-md-6">
        <input type="text" class="form-control" name="nik" autocomplete="off" required autofocus>
       </div>
      </div>

      <div class="form-group row">
       <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>

       <div class="col-md-6">
        <input type="text" class="form-control" name="name" autocomplete="off" required autofocus>
       </div>
      </div>

      <div class="form-group row">
       <label for="job" class="col-md-4 col-form-label text-md-right">Pekerjaan</label>

       <div class="col-md-6">
        <input type="text" class="form-control" name="job" autocomplete="off" required autofocus>
       </div>
      </div>

      <div class="form-group row">
       <label for="address" class="col-md-4 col-form-label text-md-right">Alamat</label>

       <div class="col-md-6">
        <textarea type="text" class="form-control" name="address"></textarea>
       </div>
      </div>

      <div class="form-group row">
       <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

       <div class="col-md-6">
        <input type="text" class="form-control" name="email" autocomplete="off" required autofocus>
       </div>
      </div>

      <div class="form-group row">
       <label for="phone" class="col-md-4 col-form-label text-md-right">No. Telp</label>

       <div class="col-md-6">
        <input type="text" class="form-control" name="phone" autocomplete="off" required autofocus>
       </div>
      </div>

      <div class="form-group row mb-0">
       <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">Tambah</button>
       </div>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div>
</div>
@endsection
